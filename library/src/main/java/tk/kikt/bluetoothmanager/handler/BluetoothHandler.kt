package tk.kikt.bluetoothmanager.handler

/**
 * Created by cai on 2017/12/14.
 */
interface BluetoothHandler {

    fun type(): BluetoothType

}